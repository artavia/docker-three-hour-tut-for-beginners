# Recap of Docker Three Hour Tut For Beginners

## Purpose
The purpose of this repo is first to underscore the importance of tagging a custom image. Then, I want to bring to light the different manners one can run a container housing your unique custom image.

Your custom image will be based on the demo-app originally created by Nana Janashia. It is a simple ExpressJS app (controller) that makes a communication to a MongoDB database (model), then, ekes out html content served statically (view) per some contrived but illustrative circumstances.

## Organization
In order to avoid creating a hodge-podge, I would like to refer you to individual sets of instructions that are mutually exclusive from one another.

Your **first option** is to [download Ms. Janashia's original project](https://gitlab.com/nanuchi/techworld-js-docker-demo-app "link to Nana's repo") and have a bird's eye view of an *Amazon Deployment Workflow*. You would need to have a subscription with Amazon Web Services to carry her guidance out to fruition. This is where I get off the train!
  - If you go this route, then, there is no need for my copycat project at all. The project setup is complete.

Your **other option** is to [download my scaled-back version of Nana's project](https://gitlab.com/artavia/test-demo "link to my repo") and gradually enhance it with additional Docker functionality.
  - You will need to download that repository and presume that *the **test-demo** folder* is your **$PWD (e.g. - present working directory)**.

## Project setup
This repo is not your project folder. This repo presents instructions, commentary and example files that will be *"replicated"* or *"rebuilt"* in your **real project folder** or **$PWD (e.g. - present working directory)**. 

## The video at YouTube
You can find [the video lesson by Nana](https://www.youtube.com/watch?v=3c-iBn73dDE "Link to youtube") at YouTube.

## Concepts and assumptions
In all cases, you will have signed up for a free-tier account at [Docker Hub](https://hub.docker.com/ "link to docker hub").
You will incrementally tag your custom images locally (e.g. - 1.0, 1.1, etc.) as opposed to using the generic "latest" tag for better tracking.
Then, you will re-tag your custom local image and prepare it for publication to your own docker-hub account.

In the end, the **main objective** is to turn the ExpressJS app into an image and consequently set up a container based on that image, and facilitate communication with two other containers. There are several ways to achieve this communication between all moving pieces but they have their differences:

  - *Method 1 - using **docker network** and **docker run** commands.* This is the only method that requires **any adjustment to the source files** in the */app/server.js* file to be precise. The value of **const CONNECTION_URL** needs to be toggled appropiately by uncommenting and commenting lines 16 and 19, respectively. In this case, you will **not** build an image and in order to whet your palette you will succeed in having your express app communicate with your db server which will be running on an image of it's own per the original instructions. Here is the link to the [Method 1](https://gitlab.com/artavia/docker-three-hour-tut-for-beginners/-/blob/master/markdown_assets/method-1.md) notes.

  - *Method 2 - using **docker login**, **docker tag**, **docker network**, **docker build** and **docker run** commands.* This method requires **no change to the source files**. Here is the link to the [Method 2](https://gitlab.com/artavia/docker-three-hour-tut-for-beginners/-/blob/master/markdown_assets/method-2.md) notes.

  - *Method 3 - using **docker login** and **docker-compose** commands.* This method requires **no change to the source files**. Here is the link to the [Method 3](https://gitlab.com/artavia/docker-three-hour-tut-for-beginners/-/blob/master/markdown_assets/method-3.md) notes.

Lord permitting, each of the results will be published to my own [Docker Hub Profile](https://hub.docker.com/r/donlucho/test-demo/tags "link to dockerhub") page in order to act as a guide.

## Attribution to Nana-chwan!!!
I just wanted to acknowledge Nana Janashia and say "thank you" again.

## Impertinent memory from the past
Every so often I think on innocuous events of the past. One of those times that I reflect on was a response I gave to a question posed by my senior year high school English teacher. Her question very innocently was "What is the meaning of life?" There were about 30 people in the class and everybody was chomping at the bit to throw in their 2 cents. 

I remember in my naivete replying that "the meaning of life is to follow the truth and serve the Lord our God who is in heaven through his only begotten son, Jesus Christ." 

It got dead silent in the classroom afterward and jaws were left agape with the exception of the class smart-ass whose name was "Brett." 

Although, he was the quintessential smart-ass and likely an atheist, he had a huge grin going from ear-to-ear when everybody else was silent and aghast. What was unique about "Brett" was that he was an **American goth** not to be confused at your own peril with a **Costa Rican goth**. The former in my experience is not a closet satanist but the latter is a satan worshiper. He wore the combat boots, trenchcoat, black denim, etc. He lived it during the late 80s. "Brett" was a genuine character and you always knew what you were going to get with him. "Brett" was a thespian back then or as we called them "a theater lesbian." 

I will never forget his reaction because for a moment I knew in my heart that *we were on the same page*. I suspect that in that moment I planted a seed not knowing, then, that one day-- all glory to God --the Lord God would give the increment to "Brett" at a time of his choosing so that the Lord Jesus Christ who is God would receive the praise.

I am as passionate about *my Lord and savior* today as ever: Jesus Christ is the *truth*, the *life* and *the way*. Give your life to Jesus Christ today for not one of us are guaranteed life tomorrow.

Where is your faith in God? If you are reading this message, then, may Jesus bless you!