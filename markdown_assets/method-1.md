# Directions for Imageless App

# Description
In this mini-lesson, we will not create a tagged image for upload. Rather, we will get each the **mongo-express** &amp; **mongo** images, plus, the running ExpressJS app to successfully communicate with one another.

## Navigate to $PWD
  - Download the repo and navigate to the root of your $PWD (e.g. - present working directory);

## Amend $PWD/app/server.js
  - Adjust **const CONNECTION_URL**; ensure that line 16 is uncommented and that line 19 is commented.

## Initialize the Docker service, if necessary
```sh
$ sudo systemctl start docker
```

## Open a new tab in terminal and install the ExpressJS app dependencies
Press [ ctrl + shift + t ] to open a new terminal. Navigate to /app **($PWD/app)**.

If you are using yarn, run:
```sh
$ yarn
```

If you are using npm, run: 
```sh
$ npm install
```

## Pull in two images as they apply to our app
Changing our focus back to our **root directory** **($PWD/)**, make preparations to import the first two images.
For more information, you can visit either of these two URLs:
  - more on [mongo-express](https://hub.docker.com/_/mongo-express "link to mongo-express image page")
  - more on [mongo](https://hub.docker.com/_/mongo "link to mongo image page")

Take an inventory of your present images on your machine:

```sh
$ sudo docker images -a
```

Then, one at a time pull in each of the two images:

```sh
$ sudo docker pull mongo
$ sudo docker pull mongo-express
```

## Create a docker network 
From each of the images, you will build a container that runs each individual image. 

But first you need to generate a Docker network facilitate communication between the ExpressJS app and the two image based containers you will build immediately afterwards.

Take inventory of your Docker networks, then, create a new Docker network called "mongo-network":
```sh
$ sudo docker network ls
$ sudo docker network create mongo-network
```

Eventually, when you are through with the custom network, you can discard it but only after you are through with any **stopped** containers (as opposed to **running** containers). **Do not discard the network just yet**

```sh
$ sudo docker network rm <<name-of-your-container>>
```

## Create a container for each mongo and mongo-express
With your Docker network called "mongo-network" in hand, now you can bring to life two new running containers. Soon, your ExpressJS app will communicate with at least one of these two containers that you are about to animate.

Take inventory of **all** your Docker containers with the following command:
```sh
$ sudo docker ps -a
```

Next, create a new Docker container for your "mongo" image called "mongodb":
```sh
$ sudo docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --network mongo-network --name mongodb mongo
```

You just created a container that does the following:
  - it runs in detached mode (-d); you have configured the ports (-p); you set the ENV vars (-e); you assigned the Docker Network called 'mongo-network' to it (&#45;&#45;network); and, you have call the container "mongodb" (&#45;&#45;name)

Then, create a new Docker container for your "mongo-express" image called "mongo-express":
```sh
$ sudo docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password -e ME_CONFIG_MONGODB_SERVER=mongodb --network mongo-network --name mongo-express mongo-express
```
You just created a container that does the following:
  - it runs in detached mode (-d); you have configured the ports (-p); you set the ENV vars (-e); you assigned the Docker Network called 'mongo-network' to it (&#45;&#45;network); and, you have call the container "mongo-express" (&#45;&#45;name)

## Lack of data-persistence in your mongodb database
For this portion of the exercise, your database will not persist, hence, it has to be recreated after your container has been stopped. **That will be addressed at another point.**

## Navigate to mongo-express
In your favorite web browser, navigate to **http://localhost:8081/**, then, do the following:
  - Locate the form field next to the "+Create Database" button, and apply the value of "**my-db**". Click to create. If you consult $PWD/app/server.js at lines 54 and 83, you will see a reference to the **my-db** database.

  - Then, click on the **my-db** (or the "View" button to its left) link under databases. 
  - Locate the form field next to the "+Create collection" button, and apply the value of "**users**". If you consult $PWD/app/server.js at lines 57 and 88, you will see a reference to the **users** collection.

## Open a new tab in terminal that points to your ExpressJS app
Press [ ctrl + shift + t ] to open a new terminal, if necessary. After you have navigated to /app **($PWD/app)**, execute the localdev command, and consequently navigate in your favorite web browser to **http://localhost:3000/**.

Run the appropriate command depending on how you installed your dependencies:
```sh
# [either...]
$ yarn run localdev
# [or...]
$ npm run localdev
```
## Let's tear this playhouse down

We will take following steps:
  - &hellip;to take inventory of "running" and "stopped" images:
    - **sudo docker ps -a** - [ Provides a list of each "running" and "stopped" containers ]
  - &hellip;to stop and remove the containers:
    - **sudo docker stop <container_id> | <name>** - [ Stops a "running" container with a valid container id or a valid container name ]
    - **sudo docker rm <container_id> | <name>** - [ Removes a container with a valid container id or a valid container name ]
  - &hellip;to take inventory of Docker networks:
    - **sudo docker network ls** - [ Provides a list of all containers including your custom container(s) ]
  - &hellip;to remove the custom Docker network:
    - **sudo docker network rm <network_id> | <name>** - [ Removes a Docker network with a valid network id or a valid network name ]
  - [ optionally ] - &hellip;to take inventory of all images:
    - **sudo docker images -a** - [ Show all images including "default" and "intermediate" ]
  - [ optionally ] - &hellip;to remove the two images (in the next lessons they will be invoked and downloaded simultaneously)
    - **sudo docker rmi [<image_id> | <image_name:image:tag>] [<image_id> | <image_name:image:tag>]** - [ Remove one or more images with valid image id or image name and image tag (e.g. -- mongo:latest, mongo-express:1.0 ) ]

## Stop the service and confirm the subsequent status
```sh
$ sudo systemctl stop docker
$ sudo systemctl status docker
```

You can press the **'q'** key in order to exit out of the systemctl status screen.

## Delete the "node_modules" folder
You can waste the "node_modules" folder and/or the corresponding lock file(s). You are not going to need them anymore from here on out. After you have navigated to /app **($PWD/app)**, execute the following command:

```sh
# [ either ]
$ rm -rf node_modules/ && rm -f yarn.lock
# [ or ]
$ rm -rf node_modules/ && rm -f package-lock.json
```

## You are all done!
If you made it this far, I thank you!