# Directions for Image-based App

# Description
In this mini-lesson, we will create a tagged image (based on the ExpressJS app) called **<<your-dockerhub-username>>/test-demo:1.1** for upload to Docker Hub. Each the **mongo-express**, **mongo** and **<<your-dockerhub-username>>/test-demo:1.1** images will successfully communicate with one another.


## Let me 'splain,'' Lucy! Let me 'splain!'
We are going to create an image of our ExpressJS app and apply a tag number of **1.1**; We have to stuff this image like a turkey with a NodeJS version of our choosing, as well as, our ExpressJS app. 

For this task, we will create **two additional files** including a **.dockerignore** file and a **Dockerfile**, per se. The latter is like a blueprint for the image you are about to create and the former is a list of files and/or directories that *the blueprint* should and can ignore. These two files can be located anywhere but their names cannot change to the best of my knowledge.


## Navigate to $PWD
  - Download the repo and navigate to the root of your $PWD (e.g. - present working directory);

## Leave $PWD/app/server.js "as is"
  - *Do not* adjust **const CONNECTION_URL**; ensure that line 16 is commented and that line 19 is uncommented.

## Initialize the Docker service, if necessary
```sh
$ sudo systemctl start docker
```

## Create new repository in Docker Hub
If you have not done so by now, sign up for a Docker account and/or sign-in to your account, then, create a new repository called "test-demo".

  - Sign in to Docker Hub
  - Click "Create a Repository" on the Docker Hub welcome page:
  - Name it "test-demo". It will be accessible later by referring to <<your-dockerhub-username>>/test-demo;
  - Set the visibility to "Public";
  - Leave the Github and Bitbucket functionalities untouched since there presently is no need to configure them;
  - Click "Create". 

## Login to Docker Hub in terminal
Login in to Docker Hub after you have ensured that your Docker service is running, then, type your password.

```sh
$ sudo docker login -u <<your-dockerhub-username>>
```

## Pull in the three images as they apply to our app
We will make preparations to import the three images.
For more information, you can visit either of these three URLs:
  - more on [mongo-express](https://hub.docker.com/_/mongo-express "link to mongo-express image page")
  - more on [mongo](https://hub.docker.com/_/mongo "link to mongo image page")
  - more on [node:14-alpine](https://hub.docker.com/_/node "link to node image page")

Take an inventory of your present images on your machine:

```sh
$ sudo docker images -a
```

Then, one at a time pull in ~~each of the three~~ two images (the Dockerfile will handle the node:14-alpine image for us):

```sh
$ sudo docker pull mongo
$ sudo docker pull mongo-express
```

## Navigate to $PWD
  - Download the repo and navigate to the root of your $PWD (e.g. - present working directory);

## Create the new files in your root directory
While in your $PWD (e.g. - present working directory) open a terminal session, then, create two new files.

First, we start with the **Dockerfile**, per se:
```sh
$ cat > Dockerfile <<EOF
FROM node:14-alpine

WORKDIR /home/app

ENV PORT 3000
ENV NODE_ENV=production
ENV MONGO_DB_USERNAME=admin
ENV MONGO_DB_PWD=password

RUN mkdir -p /home/app

COPY ./app /home/app

RUN yarn

CMD ["node", "server.js"]
EOF
```

Second, we create the **.dockerignore** file:
```sh
$ cat > .dockerignore <<EOF
/*.README.md
/app/node_modules
/app/package-lock.json
/app/yarn.lock
/app/.env
/app/*.env
/app/*.dev
/app/dot.env.txt
EOF
```

## Let's build our custom image
With our **Dockerfile** blueprint in hand, we can use the **docker build** command and simulaneously apply a tag of 1.1 to our custom image.
```sh
$ sudo docker build -t test-demo:1.1 .
```

## Open a new tab and take inventory of your images
Take an inventory of your present images on your machine:

```sh
$ sudo docker images
```

At this point you should see an entries for test-demo:1.1 , node:14-alpine, mongo:latest and mongo-express:latest . 

## Once more create a docker network and containers for test-demo, mongo and mongo-express
Take inventory of your Docker networks, then, create a new Docker network called "mytestnet":
```sh
$ sudo docker network ls
$ sudo docker network create mytestnet
```

Create a new Docker container for your "mongo" image called "mongodb":
```sh
$ sudo docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --network mytestnet --name mongodb mongo
```

Create a new Docker container for your "mongo-express" image called "mongo-express":
```sh
$ sudo docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password -e ME_CONFIG_MONGODB_SERVER=mongodb --network mytestnet --name mongo-express mongo-express
```

Finally, create a container for your custom image and call it "mytest":
```sh
$ sudo docker run -d -p 3000:3000 --network mytestnet --name mytest test-demo:1.1
```

Incidentally, the environment variables should already be **baked in** courtesy of the Dockerfile instructions.

Take inventory of your **running** Docker containers 
```sh
$ sudo docker ps
```
Be sure that under the *PORTS* column that three (3) ports are assigned:
```sh
$ PORTS
$ 0.0.0.0:3000->3000/tcp
$ 0.0.0.0:8081->8081/tcp
$ 0.0.0.0:27017->27017/tcp
```

## Navigate to mongo-express
In your favorite web browser, navigate to **http://localhost:8081/**, then, do the following:
  - Locate the form field next to the "+Create Database" button, and apply the value of "**my-db**". 
  - Then, click on the **my-db** (or the "View" button to its left) link under databases. 
  - Locate the form field next to the "+Create collection" button, and apply the value of "**users**".

## Navigate to your test-demo image
Navigate in your favorite web browser to **http://localhost:3000/**.

## Gracefully stop the "running" containers
We have seen this routine before, thus, I will whip through it in the most plausible manner possible:
```sh
$ sudo docker ps -a
$ sudo docker stop mongodb mongo-express mytest
$ sudo docker rm mongodb mongo-express mytest
$ sudo docker ps -a
$ sudo docker images
```

Stop here! We are going to re-tag our local **test-demo:1.1** image and prepare to push it to our Docker Hub repository with a name similar to **<<your-dockerhub-username>>/test-demo:1.1**. Along this vein, for example, my username is *"donlucho"*, therefore, **donlucho/test-demo:1.1** would be the value I need to apply. Shall we move on?
```sh
# $ sudo docker tag test-demo:1.1 donlucho/test-demo:1.1
$ sudo docker tag test-demo:1.1 <<your-dockerhub-username>>/test-demo:1.1
```

Take an inventory again of your present images on your machine:

```sh
$ sudo docker images -a
```

Something similar to <<your-dockerhub-username>>/test-demo:1.1 should appear. Now push it to your Docker Hub repo.
```sh
# $ sudo docker push donlucho/test-demo:1.1
$ sudo docker push <<your-dockerhub-username>>/test-demo:1.1
```

## You may stop or continue depending on your level of curiosity
If there are no changes to your ExpressJS app, you are done. 
  - You can remove the local image **and** the repo image with **docker rmi**
  - Then, while you are logged in you can pull the external image aboard
    ```sh
    # $ sudo docker pull donlucho/test-demo:1.1
    $ sudo docker pull <<your-dockerhub-username>>/test-demo:1.1
    ```
  - Re-build the containers with **docker run** but change your pointer from **the local version**&hellip;
    ```sh
    $ sudo docker run -d -p 3000:3000 --network mytestnet --name mytest test-demo:1.1
    ```
  - &hellip;to **the repo version** that is already loaded on your machine
    ```sh
    $ sudo docker run -d -p 3000:3000 --network mytestnet --name mytest <<your-dockerhub-username>>/test-demo:1.1
    ```

## You have a dangling Docker network
Do not forget to destroy your custom network
```sh
$ sudo docker network ls
$ sudo docker network rm mytestnet
```

## Do not forget to logout and stop the service, if applicable
You can logout of your Docker hub session like so:
```sh
$ sudo docker logout
```

And, stop the docker service and confirm the subsequent status:
```sh
$ sudo systemctl stop docker
$ sudo systemctl status docker
```

## You may be done!
But if you do, in fact, make changes to your ExpressJS app, then, you need to re-run **docker run** and re-compile your code for image packaging. Plus, you would need to re-tag the minor version accordingly from **1.1** to **1.2** for obvious reasons.

And, if you have had enough for one sitting, then, you can move on to the final and **most interesting** part of the lesson. I thank you!