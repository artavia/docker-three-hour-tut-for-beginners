# Directions for Image-based App

# Description
In this mini-lesson, we will create a tagged image (based on the ExpressJS app) called **<<your-dockerhub-username>>/test-demo:1.2** for upload to Docker Hub. Each the **mongo-express**, **mongo** and **<<your-dockerhub-username>>/test-demo:1.2** images will successfully communicate with one another.


## Let me 'splain,'' Lucy! Let me 'splain!'
We are going to create an image of our ExpressJS app and apply a tag number of **1.2**; We have to stuff this image like a turkey with a NodeJS version of our choosing, as well as, our ExpressJS app. 

For this task, we will create **two additional files** including a **.dockerignore** file and a **Dockerfile**, per se. The latter is like a blueprint for the image you are about to create and the former is a list of files and/or directories that *the blueprint* should and can ignore. These two files can be located anywhere but their names cannot change to the best of my knowledge.

Then, since we are looking for the best *hassle-free local development* experience, we will be interested in creating an additional file called **docker-compose.yml** which communicates with our **Dockerfile** blueprint. The **docker-compose.yml** file also permits the developer to apply **data-persistence** with relative ease when compared to its equivalent with CLI directives (there is less room for errors).

**Finally**, since we are going the route of utilizing a **docker-compose.yml** file, **docker-compose** (*not **docker**, per se*) will handle the creation and destruction of our shared Docker networks, as well as, our containers that run any viable images.


## Navigate to $PWD
  - Download the repo and navigate to the root of your $PWD (e.g. - present working directory);

## Leave $PWD/app/server.js "as is"
  - *Do not* adjust **const CONNECTION_URL**; ensure that line 16 is commented and that line 19 is uncommented.

## Initialize the Docker service, if necessary
```sh
$ sudo systemctl start docker
```

## Create new repository in Docker Hub
If you have not done so by now, sign up for a Docker account and/or sign-in to your account, then, create a new repository called "test-demo".

  - Sign in to Docker Hub
  - Click "Create a Repository" on the Docker Hub welcome page:
  - Name it "test-demo". It will be accessible later by referring to <<your-dockerhub-username>>/test-demo;
  - Set the visibility to "Public";
  - Leave the Github and Bitbucket functionalities untouched since there presently is no need to configure them;
  - Click "Create". 

## Login to Docker Hub in terminal
Login in to Docker Hub after you have ensured that your Docker service is running, then, type your password.

```sh
$ sudo docker login -u <<your-dockerhub-username>>
```

## Pull in the three images as they apply to our app
We will make preparations to import the three images.
For more information, you can visit either of these three URLs:
  - more on [mongo-express](https://hub.docker.com/_/mongo-express "link to mongo-express image page")
  - more on [mongo](https://hub.docker.com/_/mongo "link to mongo image page")
  - more on [node:14-alpine](https://hub.docker.com/_/node "link to node image page")

Take an inventory of your present images on your machine:

```sh
$ sudo docker images -a
```

Then, one at a time pull in ~~each of the three~~ two images (the Dockerfile will handle the node:14-alpine image for us):

```sh
$ sudo docker pull mongo
$ sudo docker pull mongo-express
```

## Navigate to $PWD
  - Download the repo and navigate to the root of your $PWD (e.g. - present working directory);

## Create the new files in your root directory
While in your $PWD (e.g. - present working directory) open a terminal session, then, create two new files.

First, we start with the **Dockerfile**, per se:
```sh
$ cat > Dockerfile <<EOF
FROM node:14-alpine

WORKDIR /home/app

# ENV PORT 3000
# ENV NODE_ENV=production
# ENV MONGO_DB_USERNAME=admin
# ENV MONGO_DB_PWD=password

RUN mkdir -p /home/app

COPY ./app /home/app

RUN yarn

CMD ["node", "server.js"]
EOF
```

*Notice above that the **ENV VARS are commented out**.*

Second, we create the **.dockerignore** file:
```sh
$ cat > .dockerignore <<EOF
/*.README.md
/app/node_modules
/app/package-lock.json
/app/yarn.lock
/app/.env
/app/*.env
/app/*.dev
/app/dot.env.txt
EOF
```

## Create a third file in your root directory called&hellip;
We will create an additional file called **docker-compose.yml**. This is where we will **refer** to tag number of **1.2**. In fact, if we do not have our node:14-alpine image on hand, the **docker-compose** file will pull it and bring it on board. Plus, since we can assign **ENV VARS** in the **docker-compose** file, we will comment them out in the **Dockerfile**, too, for the sake of illustration. 

```sh
$ cat > docker-compose.yml <<EOF
version: '3'

services:
  my-app: 
    build:
      context: .
      dockerfile: Dockerfile
    image: my-compose-test-demo:1.2
    container_name: mycomposetest 
    ports:
      - 3000:3000
    environment:
      - PORT=3000
      - NODE_ENV=production
      - MONGO_DB_USERNAME=admin
      - MONGO_DB_PWD=password
  mongodb:
    image: mongo
    container_name: mongodb
    ports:
      - 27017:27017
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=password
    volumes:
      - mongo-data:/data/db
      - mongodb_config:/data/configdb
  mongo-express:
    image: mongo-express
    container_name: mongo-express
    ports:
      - 8081:8081
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=password
      - ME_CONFIG_MONGODB_SERVER=mongodb

volumes: 
  mongo-data:
  mongodb_config:
EOF
```

## Let's build our custom image and initialize the other two pertinent containers
With our **Dockerfile** blueprint in hand, we can use the **docker-compose** command to build another identical version of our custom image that carries a tag of 1.2.
```sh
$ sudo docker-compose -f docker-compose.yml up --build -d
```

Just to let you know if the *&#45;&#45;build* attribute is not included, then, you will see a warning in your output similar to this:
```sh
$ WARNING: Image for service my-app was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
```

## Open a new tab and take inventory of your images
Take an inventory of your present images on your machine:

```sh
$ sudo docker images
```

At this point you should see an entries for my-compose-test-demo:1.2 , node:14-alpine, mongo:latest and mongo-express:latest . 

Incidentally, the environment variables should already be **baked in** courtesy of the instructions in the **docker-compose.yml** file.

Take inventory of your **running** Docker containers 
```sh
$ sudo docker ps
```
Be sure that under the *PORTS* column that three (3) ports are assigned:
```sh
$ PORTS
$ 0.0.0.0:3000->3000/tcp
$ 0.0.0.0:8081->8081/tcp
$ 0.0.0.0:27017->27017/tcp
```

## Navigate to mongo-express
In your favorite web browser, navigate to **http://localhost:8081/**, then, do the following:
  - Locate the form field next to the "+Create Database" button, and apply the value of "**my-db**". 
  - Then, click on the **my-db** (or the "View" button to its left) link under databases. 
  - Locate the form field next to the "+Create collection" button, and apply the value of "**users**".

## Navigate to your test-demo image
Navigate in your favorite web browser to **http://localhost:3000/**.

## Gracefully stop the "running" containers
We are going to gracefully shut down the three running services with a single command. First, we will take inventory of our "running" containers, then, execute a graceful shutdown:
```sh
$ sudo docker ps
$ sudo docker-compose down
$ sudo docker ps -a
```

## Incrementally run each of the three services
Sometimes the services start out of sequence and a miscommunication takes place. This is how to avoid that!
```sh
$ sudo docker-compose -f docker-compose.yml up --build -d mongodb
$ sudo docker-compose -f docker-compose.yml up --build -d mongo-express
$ sudo docker-compose -f docker-compose.yml up --build -d my-app
```

## Revisit your test-demo image and confirm data persistence
Navigate in your favorite web browser to **http://localhost:3000/**.

Again, you can execute a graceful shutdown:
```sh
$ sudo docker-compose down
```

## Re-tag our local image called "my-compose-test-demo:1.2"
Re-tag the local image and prepare to push it to our Docker Hub repository
```sh
# $ sudo docker tag my-compose-test-demo:1.2 donlucho/test-demo:1.2
$ sudo docker tag my-compose-test-demo:1.2 <<your-dockerhub-username>>/test-demo:1.2
```

## Push the newly prepared image to Docker Hub
Now push it to your Docker Hub repo.
```sh
# $ sudo docker push donlucho/test-demo:1.2
$ sudo docker push <<your-dockerhub-username>>/test-demo:1.2
```

## Clean up persistent data
There is the question of how to clean residual data. Presently, if we were to execute the **docker-compose up** command, we would find that any previous changes to our data are present. We tidy things up with the following command!

```sh
$ sudo docker volume --help
$ sudo docker volume ls
$ sudo docker volume prune
```

After running **docker volume prune** this is what appeared afterwards:
```sh
$ Total reclaimed space: 5.462GB
```

&lsquo;Nuff said!

## Do not forget to logout, if applicable
You can logout of your Docker hub session like so:
```sh
$ sudo docker logout
```

## Perform a final spot check
Dot your "eyes" and cross your "teas"!

```sh
$ sudo docker network ls
$ sudo docker ps -a
# $ sudo docker rmi --force c83bf113b984 79a0a0502110 51d926a5599d
$ sudo docker rmi --force <image-id-001> <image-id-099>
```

## Do not forget to stop the service, if applicable
You can stop the docker service and confirm the subsequent status like so:
```sh
$ sudo systemctl stop docker
$ sudo systemctl status docker
```

Press the **"q"** key to exit the status screen.

## Now you are done!
I am still in awe that I recovered over **5 gigabytes** of disk space!!!

Who would have ever thought that Docker and MongoDB could be such memory hogs!?!?!

I thank you all again for sticking with me this far and for tagging along! 